<?php
namespace App\Email;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Email extends DB{

    public $id;
    public $name="user";
    public $email="";

    public function __construct()
    {

        parent::__construct();

    }
    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id =$data['id'];
        }
        if(array_key_exists('name',$data)){


            $this->name = $data['name'];

        }
        if(array_key_exists('email',$data)){

            $this->email = $data['email'];

        }

    }
    public function store(){
        $arrData = array($this->name,$this->email);
        $sql = "INSERT INTO Email(name,email) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::message("Data has been inserted successfully!:)");
        else
            Message::message("Your Data does not inserted.:(");

        Utility::redirect('create.php');

    }

    public function index(){


    }

}