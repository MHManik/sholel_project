<?php
namespace App\ProfilePicture;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class ProfilePicture extends DB{

    public $id;
    public $name="user";
    public $profile_picture="";
    public $target_file;

    public function __construct()
    {

        parent::__construct();

    }
    public function setData($data = NULL)
    {

        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];

            if (array_key_exists('profile_picture', $data)) {

                $this->profile_picture = $data['profile_picture'];

            }
        }
    }
        public function store(){
        $path= 'C:/xampp/htdocs/Atomic Project/src/BITM/SEIP142994/ProfilePicture/Picture/';
        $uploadfile= $path.basename($this->profile_picture);
        move_uploaded_file($_FILES['profile_picture']['tmp_name'],$uploadfile);
        $arrData = array($this->name,$this->profile_picture);
        $sql = "INSERT INTO profile_picture(name,profile_picture) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::message("Data has been inserted successfully! :)");
        else
            Message::message("Your Data does not inserted. :(");

        Utility::redirect('create.php');

    }

    }
