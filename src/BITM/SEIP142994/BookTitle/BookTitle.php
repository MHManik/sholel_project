<?php

namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB{

    public $id;
    public $book_title="";
    public $author_name="";

    public function __construct()
    {

        parent::__construct();

    }
    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id =$data['id'];
        }
        if(array_key_exists('book_title',$data)){


            $this->book_title = $data['book_title'];

        }
        if(array_key_exists('author_name',$data)){

            $this->author_name = $data['author_name'];

        }

    }
    public function store(){
        $arrData = array($this->book_title,$this->author_name);
        $sql = "INSERT INTO book_title(book_title,author_name) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::message("Data has been inserted successfully!:)");
        else
            Message::message("Your Data does not inserted.:(");

        Utility::redirect('create.php');

    }



    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from book_title');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();



    public function view($fetchMode='ASSOC'){
        $sql= 'SELECT * from book_title where id='.$this->id;
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();

    public function update()
    {
        $arrData=array($this->book_title,$this->author_name);
        $sql="UPDATE book_title SET author_name = ?, book_title = ? WHERE id =".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);


        Utility::redirect('index.php');


    }

    public function delete()
        {

            $sql ="Delete from book_title Where id =".$this->id;
            $STH=$this->DBH->prepare($sql);
            $STH->execute();
            Utility::redirect('index.php');
        }

public function trash()
                {

                    $sql="UPDATE book_title SET  is_deleted  = NOW() WHERE id =".$this->id;
                    $STH=$this->DBH->prepare($sql);
                    $STH->execute();


                    Utility::redirect('index.php');

                }

}