<?php
include_once('../../../vendor/autoload.php');
use App\SummaryOfOrganization\SummaryOfOrganization;

$objSummeryOfOrganization = new SummeryOfOrganization();

$objSummeryOfOrganization->setData($_POST);

$objSummeryOfOrganization->store();